import unittest

from dto import *


class DtoTests(unittest.TestCase):
    def test_image_size(self):
        d = {'width': 23, 'height': 34}
        image_size = ImageSize.init_from_dict(d)
        self.assertEqual(image_size.width, 23)
        self.assertEqual(image_size.height, 34)

    def test_joint_type(self):
        self.assertEqual(len(list(JointType)), 14, "Expected 14 values in JointType")
        raw_value = 'neck'
        joint_type = JointType(raw_value)
        self.assertEqual(joint_type, JointType.NECK)

    def test_point(self):
        d = {'x': 1.0, 'y': 1.5}
        point = Point.init_from_dict(d)
        self.assertEqual(point.x, 1.0)
        self.assertEqual(point.y, 1.5)

    def test_joint(self):
        d = {
            'jointType': 'head',
            'point': {
                'x': 1,
                'y': 2
            },
            'confidence': 0.25
        }
        joint = Joint.init_from_dict(d)
        self.assertEqual(joint.joint_type, JointType.HEAD)
        self.assertEqual(joint.point, Point(1, 2))
        self.assertEqual(joint.confidence, 0.25)

    def test_measurement(self):
        d = {
            'uuid': 'uuid here',
            'imageSize': {
                'width': 1,
                'height': 10.5
            },
            'imagePreview': "",
            'isGood': False,
            'joints': [
                {
                    'jointType': 'head',
                    'point': {
                        'x': 1,
                        'y': 2
                    },
                    'confidence': 0.25
                }
            ]
        }

        measurement = Measurement.init_from_dict(d)
        self.assertEqual(measurement.uuid, 'uuid here')
        self.assertEqual(measurement.image_size, ImageSize(1, 10.5))
        self.assertEqual(measurement.image_preview, '')
        self.assertEqual(measurement.is_good, False)
        self.assertEqual(type(measurement.joints), dict)
        self.assertEqual(len(measurement.joints), 1)
        self.assertTrue(JointType.HEAD in measurement.joints)

        head_joint = measurement.joints[JointType.HEAD]
        self.assertIsNotNone(head_joint)


if __name__ == '__main__':
    unittest.main()
