from dto import JointType
from dto import Point


class Joint:
    def __init__(self, joint_type: JointType, point: Point, confidence: float):
        self.joint_type = joint_type
        self.point = point
        self.confidence = confidence

    @staticmethod
    def init_from_dict(d: dict):
        joint_type_str = d['jointType']
        joint_type = JointType(joint_type_str)

        point_dict = d['point']
        point = Point.init_from_dict(point_dict)

        confidence = d['confidence']

        return Joint(joint_type=joint_type, point=point, confidence=confidence)
