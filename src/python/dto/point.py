import shapely.geometry as gm


class Point(gm.Point):
    @staticmethod
    def init_from_dict(d: dict):
        x = d['x']
        y = d['y']
        return Point(x, y)
