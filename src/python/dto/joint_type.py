from enum import Enum, unique


@unique
class JointType(Enum):
    LEFT_HIP = "lHip"
    RIGHT_HIP = "rHip"
    LEFT_SHOULDER = "lShoulder"
    RIGHT_SHOULDER = "rShoulder"
    NECK = "neck"
    LEFT_ANKLE = "lAnkle"
    RIGHT_ANKLE = "rAnkle"
    RIGHT_WRIST = "rWrist"
    LEFT_WRIST = "lWrist"
    RIGHT_ELBOW = "rElbow"
    LEFT_ELBOW = "lElbow"
    RIGHT_KNEE = "rKnee"
    LEFT_KNEE = "lKnee"
    HEAD = "head"
