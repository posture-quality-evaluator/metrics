class ImageSize:
    def __init__(self, width: float, height: float):
        self.width = width
        self.height = height

    @staticmethod
    def init_from_dict(d: dict):
        width = d['width']
        height = d['height']

        return ImageSize(width=width, height=height)

    def __eq__(self, other):
        return self.width == other.width and self.height == other.height

    def __repr__(self):
        return "ImageSize(width=%f, height%f)" % (self.width, self.height)
