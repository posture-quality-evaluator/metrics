from typing import Union, Dict, List
from dto import ImageSize, JointType, Joint


class Measurement:
    def __init__(self, uuid: str, image_preview: str, image_size: ImageSize, is_good: bool,
                 joints: Union[Dict[JointType, Joint], List[Joint]]):
        self.uuid = uuid
        self.image_preview = image_preview
        self.image_size = image_size
        self.is_good = is_good

        if isinstance(joints, list):
            joints = {j.joint_type: j for j in joints}
        self.joints = joints

    @staticmethod
    def init_from_dict(d: dict):
        uuid = d['uuid']

        image_preview = d['imagePreview']

        image_size_dict = d['imageSize']
        image_size = ImageSize.init_from_dict(image_size_dict)

        is_good = d['isGood']

        joints_dicts_list = d['joints']
        joints = [Joint.init_from_dict(joint_dict) for joint_dict in joints_dicts_list]

        return Measurement(uuid=uuid, image_preview=image_preview, image_size=image_size, is_good=is_good,
                           joints=joints)
