from metrics import Metric, MetricResult
from dto import Measurement, JointType


class HeadBodyProportionMetricResult(MetricResult):
    """
    ``HeadBodyProportionMetric`` result which contains `coef` value - attitude of body and head height
    """

    def __init__(self, coef: float):
        self.coef = coef

    def display_result(self):
        return self.coef


class HeadBodyProportionMetric(Metric):
    """
    Metric which is based on attitude of body and head height
    """

    @property
    def required_joints(self):
        return [
            JointType.HEAD,
            JointType.NECK,
            JointType.LEFT_HIP,
            JointType.RIGHT_HIP
        ]

    def compute(self, measurement: Measurement) -> HeadBodyProportionMetricResult:
        head = measurement.joints[JointType.HEAD]
        neck = measurement.joints[JointType.NECK]
        left_hip = measurement.joints[JointType.LEFT_HIP]
        right_hip = measurement.joints[JointType.RIGHT_HIP]

        hips_middle_y = (left_hip.point.y + right_hip.point.y) / 2
        head_height = head.point.y - neck.point.y
        body_height = neck.point.y - hips_middle_y

        coef = body_height / head_height
        return HeadBodyProportionMetricResult(coef=coef)

    @staticmethod
    def display_name():
        return "Head Body Proportion"
