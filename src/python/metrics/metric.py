from abc import ABC, abstractmethod
from typing import List, Any

from dto import Measurement, JointType


class MetricResult(ABC):
    """
    Base abstract class for metric's ``compute`` method return value
    """

    @abstractmethod
    def display_result(self) -> Any:
        """
        Getter of metric's result. Should be str, float, int etc.
        :return:
        """
        raise NotImplemented


class Metric(ABC):
    """
    Base abstract class for any metric
    """

    @property
    @abstractmethod
    def required_joints(self) -> List[JointType]:
        """
        Abstract property which determines necessary for metric computation joints.
        Invocation of ``compute`` method with measurement which does not have any of required joints
        will produce undefined behaviour

        :return: list of JointType instances
        """
        raise NotImplemented

    @abstractmethod
    def compute(self, measurement: Measurement) -> MetricResult:
        """
        The main method of Metric
        :param measurement: Photo markup
        :return: Computation result
        """
        raise NotImplemented

    @staticmethod
    @abstractmethod
    def display_name() -> str:
        """
        Static abstract method which must return title, describing the metric's result
        :return: str
        """
        raise NotImplemented
