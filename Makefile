# Makefile

SHELL := /bin/bash
venv_base := './.venv'
python_version := '3.7'
requirements_path := './requirements.txt'

install:
	@echo -e "\033[93mCreating virtual environment at $(venv_base)\033[0m"
	@echo "Expected python version is $(python_version) (other versions I didn't test)"
	python$(python_version) -m venv $(venv_base)

	@echo "Upgrading pip"
	source $(venv_base)/bin/activate; \
	pip install --upgrade pip;

	@echo -e "\033[93mInstalling requirements from $(requirements_path)\033[0m"
	source $(venv_base)/bin/activate; \
	pip install -r $(requirements_path);

	source $(venv_base)/bin/activate; \
	echo -e "Python version: \033[93m`python --version`\033[0m"; \
	echo -e "Python path: \033[93m`which python`\033[0m";

	@echo -e "\033[96mSuccess! Now you can test with \`make test\`\033[0m"

test:
	@echo "Testing files at: ./src/tests/"
	source $(venv_base)/bin/activate; \
	PYTHONPATH='./src/python' pytest ./src/tests/;

	@echo -e "\033[96mSuccess! Now you can start Jupyter with \`make run\`\033[0m"

freeze:
	source $(venv_base)/bin/activate; \
	pip --version; \
	pip freeze > requirements.txt;

run:
	@echo -e "\033[93mRunning JupyterHub. Visit Main notebook at src/python/Main.ipynb\033[0m"
	sleep 3
	source $(venv_base)/bin/activate; \
	jupyter notebook;
	@echo Exited Jupyter
