# Posture Metrics🧍🧍‍♀️📏

Here we test and run metrics on measurements received from iPhone. Main verticals are python, virtualenv and jupyter.

## Installation

### Unix

```shell
$ make install test
```

### Windows

Download <a href="https://ubuntu.com/download/desktop">Ubuntu</a>. Then see Unix installation.

Or if you are a profi Windows user and you know how to deal with `virtualenv`, `pytest` and `JupyterHub` you can try creating bat makefile or any other helper files needed for your OS to run the code.

## Usage

The code mostly is executed from Main.ipynb, so you need to run JupyterHub. For implemting new metrics use PyCharm (see `/src/python/metrics` directory).

### Unix

This will run JupyterHub in virtual environment.

```shell
$ make run
```

### Windows

(I don't know, you can update this guide if you do).

### PyCharm

The project is ready to be used in PyCharm where you can run tests from or even edit Jupyter notebook. Also it's expected that you will be developing metrics using PyCharm.

If after cloning you face a problem of importing `dto` module, see troubleshooting <a href="https://gitlab.com/posture-quality-detection/metrics/-/issues/1">here</a>.

## Data

The measurements were received from iPhone's app `Pose Demo` which uses neural network to find joints on image with human. Main.ipynb notebook expects file `measurements.json` to be in _/data_ directory.

A measurement contains:
- `uuid`. String unique identifier of measurement
- `imagePreview`. Base64 string of image (150px, 150px)
- `imageSize`. Object with keys keys: _width_, _height_
- `isGood`. Boolean label indicating whether posture on image is good (markuped manually)
- `joints`. Array of objects:
    - `joint_type`. Enumerated value (see `/src/python/dto/joint_type.py`)
    - `point`. Object with keys: _x_, _y_. X grows right, Y grows up
    - `confidence`. Confidence of NN in this joint

DTO classes (see `/src/python/dto/`) already implement data layout.

## Metrics

File `/src/python/metrics/metric.py` contains two classes:
- `Metric`. Base abstract class of metric with methods:
    - `required_joints`. Must return list of JointType indicating which joints the metric will use. Passing to _compute_ measurement without required set of joints produces undefined behaviour
    - `compute`. Main method wich must take measurement, calculate and return subclass of MetricResult
    - `display_name`. Returned string is used for constructing a dataframe from results
- `MetricResult`. Base abstract class of metric computation result with methods:
    - `display_result`. Return value is used in dataframe. Expected values are _str_, _int_, _float_ etc.

Any new metric must inherit `Metric` class and implement abstract methods. Example is `HeadBodyProportionMetric`.

`Main.ipynb` implements simple execution of metrics on measurements.
